#### Installation guide

- install package dependencies

```
pip install -r requirements.txt
```

- run the server

```
python manage.py runserver
```

- run the consumer

```
python consumer.py
```
