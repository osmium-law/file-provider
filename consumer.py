import pika
import json
import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'osmium_provider.settings')
django.setup()

from api.models import File

credentials = pika.PlainCredentials('osmium', 'osmium12345678')
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='rabbitmq.faishol.net', credentials=credentials))
channel = connection.channel()
channel.queue_declare(queue='provider_queue')


def callback(ch, method, properties, body):
    print("Received in provider...")
    data = json.loads(body)

    if data["query_type"] == 'update_progress':
        try:
            req_file = File.objects.get(id=data['id'])
            req_file.progress = data['progress']
            req_file.save()
            print("progress updated")
        except:
            print(f"object with id {data['id']} doesn't exist")
    elif data["query_type"] == 'update_url':
        try:
            req_file = File.objects.get(id=data['id'])
            req_file.file = data['url']
            req_file.save()
            print("url updated")
        except:
            print(f"object with id {data['id']} doesn't exist")
    elif data["query_type"] == "create":
        req_file = File(user_code=data["user_code"], id=data["id"])
        req_file.save()
        print("object created")
    else:
        print(data)


channel.basic_consume(
    queue='provider_queue', on_message_callback=callback, auto_ack=True)
print("Started Consuming...")
channel.start_consuming()
