from django.db import models


class File(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    time_request = models.DateTimeField(auto_now_add=True)
    user_code = models.CharField(max_length=30)
    progress = models.CharField(max_length=100, null=True, blank=True)
    file = models.CharField(max_length=300, null=True, blank=True)
