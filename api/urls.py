from django import views
from django.urls import path
from . import views

urlpatterns = [
    path('list/', views.list_file, name='list_file'),
    path('redeem/<str:token>/', views.redeem_token, name='redeem_token'),
    path('<str:id>/', views.retrieve, name='retrieve file'),
]
