from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import requests
from django.http import Http404, HttpResponse
from django.core.cache import cache
from django.urls import reverse
from django.views.decorators.cache import cache_page

import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter


from .models import File
from .serializers import FileSerializer

import os
from uuid import uuid4

logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)

# Create the handler
handler = AsynchronousLogstashHandler(
    host='34.132.46.126',
    port=5000,
    ssl_enable=False,
    ssl_verify=False,
    database_path='')
# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)


def verify_token(access_token):
    r = requests.get(
        'https://osmium-auth.herokuapp.com/users/verify-token/' + access_token + '/')
    if r.status_code == 200:
        return r.json()['data']['user']
    else:
        logger.error("[PROVIDER]: Token not valid")
        return None


def get_token(header):
    return header['HTTP_AUTHORIZATION']

@api_view(['GET'])
def list_file(request):
    try:
        access_token = request.META['HTTP_AUTHORIZATION']
        if (access_token == None):
            return Response({"error": "Request not valid"}, status=status.HTTP_403_FORBIDDEN)
        access_token = access_token.split(" ")[1]

    except:
        return Response({"error": "Request not valid"}, status=status.HTTP_403_FORBIDDEN)

    res = verify_token(access_token)

    if res is not None:
        list_file = File.objects.filter(
            user_code=res)

        serializer = FileSerializer(list_file, many=True)
        logger.info("[PROVIDER]: List files" + res)

        return Response({"results": serializer.data})

    return Response({"error": "Request not valid"}, status=status.HTTP_403_FORBIDDEN)


@api_view(['GET'])
def retrieve(request, id):
    try:
        access_token = request.META['HTTP_AUTHORIZATION']
        if (access_token == None):
            return Response({"error": "Request not valid"}, status=status.HTTP_403_FORBIDDEN)
        access_token = access_token.split(" ")[1]

    except:
        return Response({"error": "Request not valid"}, status=status.HTTP_403_FORBIDDEN)

    res = verify_token(access_token)

    if res is not None:
        list_file = File.objects.filter(pk=id)
        serializer = FileSerializer(list_file, many=True)
        
        requested_file = list_file.first()
        token_file = generate_token_file(requested_file.file)
        download_url = ""
        if len(token_file) > 0:
            download_url = os.getenv("HOST_IP", "localhost") + reverse('redeem_token', args=[token_file])
            
        logger.info("[PROVIDER]: get file")
        return Response({"results": serializer.data, "download_url": download_url})

    return Response({"error": "Request not valid"}, status=status.HTTP_403_FORBIDDEN)

def generate_token_file(outfile):
    if not outfile or outfile == "":
        return ""
    kode = str(uuid4())
    cache.set(f"file-{kode}", outfile.encode(), 60 * 60)
    return kode

def redeem_token(request, token):
    filepath = cache.get(f"file-{token}")
    if filepath and os.path.exists(filepath):
        cache.delete(f"file-{token}")
        with open(filepath, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/octet-stream")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(filepath.decode())
            return response
    else:
        raise Http404
